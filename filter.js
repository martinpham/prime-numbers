const primeNumbers = [2];

const total = 1000000;

let calculated = 0;

/*
n = a * b (a,b >= 1)
m < n, m = x * y

n % m == 0 -> n % x == 0 & n % y == 0

-> only check prime number < n




n = a * b (a,b >= 1)
sqrtn * sqrtn = a * b
a < sqrtn -> b > sqrtn
a > sqrtn -> b < sqrtn
a = sqrtn -> b = sqrtn

-> a or b <= sqrtn

-> always exists a number <= sqrtn

-> only check number <= sqrtn
*/

for(let i = 3; primeNumbers.length < total; i+= 2)
{
	let isPrime = true;
	
	calculated++;
	let sqrti = Math.sqrt(i);
	
	for(let j = 0; primeNumbers[j] <= sqrti; j++)
	{
		const primeNumber = primeNumbers[j];
		
		calculated++;
		if(i % primeNumber === 0)
		{
			isPrime = false;
			break;
		}
	}
	
	if(isPrime)
	{
		primeNumbers.push(i);
	}
}

console.log(primeNumbers, calculated);
